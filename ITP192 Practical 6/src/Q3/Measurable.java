package Q3;

public interface Measurable {
	double getMeasure();
}
