package Q1;
import java.util.ArrayList;

public class PersonApp {
	public static double calAvgMark(ArrayList<Person> plist) {
		double total = 0;
		int cntStudent = 0;
		
		for (Person p : plist) {
			if (p instanceof Student) {
				total += ((Student) p).computeFinalMark();
				cntStudent++;
			}
		}
		
		return total;
	}

	public static void main(String[] args) {
		ArrayList<Person> personlist = new ArrayList<Person>();
		
		Lecturer lect1 = new Lecturer("1234567A", "Phoon Lee Kien", "1234D");
		lect1.setTotalTeachingHr(30);
		personlist.add(lect1);
		
		Lecturer lect2 = new Lecturer("1234567B", "John Tan", "1234E");
		lect2.setTotalTeachingHr(35);
		personlist.add(lect2);
		
		Lecturer lect3 = new Lecturer("1234567C", "Lydia Neo", "1234F");
		lect3.setTotalTeachingHr(35);
		personlist.add(lect3);
		
		Student stud1 = new Student("1231234B", "Tan Mei Mei", "120101F");
		stud1.setTestMark(30);
		stud1.setExamMark(100);
		personlist.add(stud1);
		
		Student stud2 = new Student("1231234A", "James Tan", "120101E");
		stud2.setTestMark(40);
		stud2.setExamMark(90);
		personlist.add(stud2);
		
		Student stud3 = new Student("1231234C", "John Lim", "120101G");
		stud3.setTestMark(50);
		stud3.setExamMark(80);
		personlist.add(stud3);
		
		Student stud4 = new Student("1231234D", "May Lee", "120101H");
		stud4.setTestMark(60);
		stud4.setExamMark(70);
		personlist.add(stud4);
		
		for (int i = 0 ; i < personlist.size(); i++) {
			personlist.get(i).view();
		}
		
		System.out.print("Average Mark of students: " + calAvgMark(personlist));
	}
}
